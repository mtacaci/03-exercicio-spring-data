package com.itau.funcionarios.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itau.funcionarios.models.Departamento;

import com.itau.funcionarios.repository.InterfaceDepartamento;

@Service
public class DepartamentoService {
	@Autowired
	InterfaceDepartamento interfaceDepartamento;

	public Iterable<Departamento> listarDepartamento() {
		return interfaceDepartamento.findAll();
	}

}

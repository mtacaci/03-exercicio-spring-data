package com.itau.funcionarios.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itau.funcionarios.models.Localizacao;
import com.itau.funcionarios.repository.InterfaceLocalizacao;

@Service
public class LocalizacaoService {
@Autowired 
InterfaceLocalizacao interfaceLocalizacao;

	public Iterable<Localizacao> listarLocalizacao() {
		return interfaceLocalizacao.findAll();
	}
	
}

package com.itau.funcionarios.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itau.funcionarios.models.Funcionario;
import com.itau.funcionarios.repository.InterfaceFuncionario;

@Service
public class FuncionarioService {
	@Autowired
	InterfaceFuncionario interfaceFuncionario;

	public Iterable<Funcionario> listarFuncionario() {
		return interfaceFuncionario.findAll();
	}
	
	public void incluirCargo(Funcionario funcionario) {
		interfaceFuncionario.save(funcionario);
	}
	
	public void deletarCargo(int id) {
		interfaceFuncionario.deleteById(id);
	}

}

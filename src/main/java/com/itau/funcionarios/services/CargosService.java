package com.itau.funcionarios.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itau.funcionarios.models.Cargos;
import com.itau.funcionarios.repository.InterfaceCargos;

@Service
public class CargosService {
	@Autowired
	InterfaceCargos interfaceCargos;

	public Iterable<Cargos> listarCargo() {
		return interfaceCargos.findAll();
	}
	
	public void incluirCargo(Cargos cargo) {
		interfaceCargos.save(cargo);
	}

}

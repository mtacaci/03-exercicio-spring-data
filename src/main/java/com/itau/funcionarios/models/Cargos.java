package com.itau.funcionarios.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "jobs")
public class Cargos {
	@Column(name = "job_id")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name = "job_title")
	private String nome;
	//@Column(name = "min_salary", columnDefinition="decimal", precision=8, scale=2)
	@Column(name = "min_salary")
	private Double valorMinimoSalario;
	//@Column(name = "max_salary", columnDefinition="decimal", precision=8, scale=2)
	@Column(name = "max_salary")
	private Double valorMaximoSalario;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Double getValorMinimoSalario() {
		return valorMinimoSalario;
	}

	public void setValorMinimoSalario(Double valorMinimoSalario) {
		this.valorMinimoSalario = valorMinimoSalario;
	}

	public Double getValorMaximoSalario() {
		return valorMaximoSalario;
	}

	public void setValorMaximoSalario(Double valorMaximoSalario) {
		this.valorMaximoSalario = valorMaximoSalario;
	}

}

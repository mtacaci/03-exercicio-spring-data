package com.itau.funcionarios.models;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "employees")
public class Funcionario {
	@Column(name = "employee_id")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name = "first_name")
	private String nomePrim;
	@Column(name = "last_name")
	private String nomeUlt;
	private String email;
	@Column(name = "phone_number")
	private String telefone;
	@Column(name = "hire_date")
	private LocalDate dataContratacao;
	@JoinColumn(name = "job_id")
	@ManyToOne
	@NotNull
	private Cargos idCargo;
	@Column(name = "salary")
	@NotNull
	private double salario;
	@JoinColumn(name = "department_id")
	@ManyToOne
	@NotNull
	private Departamento idDepartamento;
	@JoinColumn(name = "manager_id")
	@ManyToOne
	private Funcionario gerente;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNomePrim() {
		return nomePrim;
	}

	public void setNomePrim(String nomePrim) {
		this.nomePrim = nomePrim;
	}

	public String getNomeUlt() {
		return nomeUlt;
	}

	public void setNomeUlt(String nomeUlt) {
		this.nomeUlt = nomeUlt;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public LocalDate getDataContratacao() {
		return dataContratacao;
	}

	public void setDataContratacao(LocalDate dataContratacao) {
		this.dataContratacao = dataContratacao;
	}

	public Cargos getIdCargo() {
		return idCargo;
	}

	public void setIdCargo(Cargos idCargo) {
		this.idCargo = idCargo;
	}

	public Departamento getIdDepartamento() {
		return idDepartamento;
	}

	public void setIdDepartamento(Departamento idDepartamento) {
		this.idDepartamento = idDepartamento;
	}

	public Funcionario getGerente() {
		return gerente;
	}

	public void setGerente(Funcionario gerente) {
		this.gerente = gerente;
	}

	public double getSalario() {
		return salario;
	}

	public void setSalario(double salario) {
		this.salario = salario;
	}

}

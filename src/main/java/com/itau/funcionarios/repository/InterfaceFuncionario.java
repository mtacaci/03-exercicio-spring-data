package com.itau.funcionarios.repository;

import org.springframework.data.repository.CrudRepository;

import com.itau.funcionarios.models.Funcionario;

public interface InterfaceFuncionario extends CrudRepository<Funcionario, Integer> {

}

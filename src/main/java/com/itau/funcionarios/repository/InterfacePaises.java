package com.itau.funcionarios.repository;

import org.springframework.data.repository.CrudRepository;

import com.itau.funcionarios.models.Paises;

public interface InterfacePaises extends CrudRepository<Paises, String> {

}

package com.itau.funcionarios.repository;

import org.springframework.data.repository.CrudRepository;

import com.itau.funcionarios.models.Cargos;

public interface InterfaceCargos extends CrudRepository<Cargos, Integer> {

}

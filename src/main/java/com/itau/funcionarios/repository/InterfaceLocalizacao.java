package com.itau.funcionarios.repository;

import org.springframework.data.repository.CrudRepository;

import com.itau.funcionarios.models.Localizacao;

public interface InterfaceLocalizacao extends CrudRepository<Localizacao, Integer> {

}

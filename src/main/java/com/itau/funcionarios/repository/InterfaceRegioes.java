package com.itau.funcionarios.repository;

import org.springframework.data.repository.CrudRepository;

import com.itau.funcionarios.models.Paises;
import com.itau.funcionarios.models.Regioes;

public interface InterfaceRegioes extends CrudRepository<Regioes, Integer> {

}

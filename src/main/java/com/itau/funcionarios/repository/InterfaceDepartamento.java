package com.itau.funcionarios.repository;

import org.springframework.data.repository.CrudRepository;

import com.itau.funcionarios.models.Departamento;

public interface InterfaceDepartamento extends CrudRepository<Departamento, Integer> {

}

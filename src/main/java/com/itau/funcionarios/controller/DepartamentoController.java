package com.itau.funcionarios.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.itau.funcionarios.models.Departamento;
import com.itau.funcionarios.models.Localizacao;
import com.itau.funcionarios.services.DepartamentoService;
import com.itau.funcionarios.services.LocalizacaoService;

@RestController
@RequestMapping("/departamento")
public class DepartamentoController {
	@Autowired
	DepartamentoService departamentoService;
	
	@GetMapping
	public Iterable<Departamento> listarLocalizacoes() {
		return departamentoService.listarDepartamento();
	}

}

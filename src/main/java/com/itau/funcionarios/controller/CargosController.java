package com.itau.funcionarios.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.itau.funcionarios.models.Cargos;
import com.itau.funcionarios.models.Departamento;
import com.itau.funcionarios.models.Localizacao;
import com.itau.funcionarios.services.CargosService;
import com.itau.funcionarios.services.DepartamentoService;
import com.itau.funcionarios.services.LocalizacaoService;

@RestController
@RequestMapping("/cargos")
public class CargosController {
	@Autowired
	CargosService cargosService;
	
	@GetMapping
	public Iterable<Cargos> listarLocalizacoes() {
		return cargosService.listarCargo();
	}
	
	@PostMapping
	public void listarLocalizacoes(@RequestBody Cargos cargo) {
		cargosService.incluirCargo(cargo);
	}

}
